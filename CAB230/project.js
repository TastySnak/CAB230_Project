/*///////////////////////////
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
///////////////////////////*/

// declare variables
var modal;

///////////////////////
/* Global javascript */
///////////////////////

function off_alert(this_field) {
    this_field.style.borderColor = "#666666";
}

////////////////////////////
/* Signup page javascript */
////////////////////////////

function signup_submit() {
    // Upper cases
    var upper = /[A-Z]/;

    // Lower cases
    var lower = /[a-z]/;

    // Numbers
    var numeric = /[0-9]/;

    // Symbols
    var symbol = /\W/;

    // Space
    var space = /\s/;

    // The minimum length of Passwords.
    var min_length = 8;

    // Validation flags
    var is_valid = true;
    var username_blank = password1_blank = password2_blank = givenname_blank = surname_blank = email_blank = false;
    var username_valid = password1_valid = password2_valid = givenname_valid = surname_valid = email_valid = dob_valid = checkbox_valid = true;

    // variable declarations & initialisations
    var username = document.forms["reg-form"]["username"].value;
    var password1 = document.forms["reg-form"]["password1"].value;
    var password2 = document.forms["reg-form"]["password2"].value;
    var givenname = document.forms["reg-form"]["given"].value;
    var surname = document.forms["reg-form"]["last"].value;
    var email = document.forms["reg-form"]["email"].value;
    document.getElementById('signup-validation-info').innerHTML = 'Please correct the following errors:';
  

    // emails validation.
    var emailValidation = /^(([^*()\[\]\\.,;:\s@"]+(\.[^*()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var checkbox = document.forms["reg-form"]["check"].checked;
    var day = document.forms["reg-form"]["day"].value;
    var month = document.forms["reg-form"]["month"].value;
    var year = document.forms["reg-form"]["year"].value;
    var is_leapyear = check_leapyear(year);

    // This section checks username, password1, password2, givenname, surname, and email values if they are empty.
    // (Patterns = 6C1 + 6C2 + 6C3 + 6C4 + 6C5)
    // The searching borderColor makes easy to find every pattern.

    if (username == "") {
        document.getElementById("uname-field").style.borderColor = "red";
        is_valid = false;
    }
    if (password1 == "") {
        document.getElementById("passwd1-field").style.borderColor = "red";
        is_valid = false;
    }
    if (password2 == "") {
        document.getElementById("passwd2-field").style.borderColor = "red";
        is_valid = false;
    }
    if (givenname == "") {
        document.getElementById("fname-field").style.borderColor = "red";
        is_valid = false;
    }
    if (surname == "") {
        document.getElementById("lname-field").style.borderColor = "red";
        is_valid = false;
    }
    if (email == ""){
        document.getElementById("email-field").style.borderColor = "red";
        is_valid = false;
    }

    // Check whether or not the checkbox is checked.
    if (!checkbox){
        var paragraph = document.createElement("P");
        var text = document.createTextNode("* You must agree to our terms and conditions.");
        paragraph.appendChild(text);
        document.getElementById("signup-validation-info").appendChild(paragraph);
        document.getElementById("c_box").style.outline = '1px solid #ff0000';
        is_valid = false;
        checkbox_valid = false;
    }
    else {
        document.getElementById("c_box").style.outline = '1px solid #666666';
        checkbox_valid = true;
    }

    // Validate the Date of Birth
    // February, depending on whether or not the year is the leap year.
    if ((month == 2) && (!is_leapyear) && (day > 28)) {
        document.getElementById("date-fields").style.borderColor = "red";
        is_valid = false;
        dob_valid = false;
    } else if ((month == 2) && (is_leapyear) && (day > 29)){
        document.getElementById("date-fields").style.borderColor = "red";
        is_valid = false;
        dob_valid = false;
    }

    // Check if days do not exceed certain months
    if (((month == 4) || (month == 6) || (month == 9) || (month == 11)) && (day == 31)) {
        document.getElementById("date-fields").style.borderColor = "red";
        is_valid = false;
        dob_valid = false;
    }

    // Check that year is not too close to current
    var current_date = new Date();
    var current_year = current_date.getFullYear();
    if ((current_year - year < 5) || (current_year - year > 100)) {
        document.getElementById("date-fields").style.borderColor = "red";
        is_valid = false;
        dob_valid = false;
    }

    // Turn of warning if date of birth is valid
    if (dob_valid) {
        document.getElementById("date-fields").style.borderColor = "#666666";
    } else {
        var paragraph = document.createElement("P");
        var text = document.createTextNode("* Please enter a valid date of birth.");
        paragraph.appendChild(text);
        document.getElementById("signup-validation-info").appendChild(paragraph);
    }

    // Return if fields are left blank
    if (!is_valid) {
        var paragraph = document.createElement("P");
        var text = document.createTextNode("* You must complete all the information.");
        paragraph.appendChild(text);
        document.getElementById("signup-validation-info").appendChild(paragraph);
        s_form = document.getElementById("signup-form");
        s_form.style.display = "none";
        modal = document.getElementById("signup-validation-modal");
        modal.style.display = "block";
        return false;
    }
    

    // This section checks username, password1, password2, givenname, surname, and email values if they are invalid.
    // The searching borderColor makes easy to find every pattern.

    // username check
    if (username.length < min_length) {
        document.getElementById("uname-field").style.borderColor = "red";
        is_valid = false;
        username_valid = false;
    }

    if ((password1.length < min_length) || (!(upper.test(password1))) || (!(lower.test(password1))) || (!(numeric.test(password1))) || (!(symbol.test(password1))) || (space.test(password1))) {
        document.getElementById("passwd1-field").style.borderColor = "red";
        is_valid = false;
        password1_valid = false;
    }

    if (!(password1 == password2)) {
        document.getElementById("passwd2-field").style.borderColor = "red";
        is_valid = false;
        password2_valid = false;
    }

    if ((!(upper.test(givenname))) || (!(lower.test(givenname)))) {
        document.getElementById("fname-field").style.borderColor = "red";
        is_valid = false;
        givenname_valid = false;
    }

    if ((!(upper.test(surname))) || (!(lower.test(surname)))) {
        document.getElementById("lname-field").style.borderColor = "red";
        is_valid = false;
        surname_valid = false;
    }

    if (!(emailValidation.test(email))) {
        document.getElementById("email-field").style.borderColor = "red";
        is_valid = false;
        email_valid = false;
    } 

    var validation_flags =  [username_valid, password1_valid, password2_valid, givenname_valid, surname_valid, email_valid, dob_valid, checkbox_valid];

    // Get validation errors
    if (!is_valid) {
        for (var i = 0; i < validation_flags.length; i++) {
            if (!validation_flags[i]) {
                switch(i) {
                    case 0:
                        var paragraph = document.createElement("P");
                        var text = document.createTextNode("* Username must be at least 8 characters.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);
                        break;
                        
                    case 1:
                        var paragraph = document.createElement("P");
                        var text = document.createTextNode("* Password must contain at least one uppercase character.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);
                        
                        paragraph = document.createElement("P");
                        text = document.createTextNode("* Password must contain at least one lowercase character.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);

                        paragraph = document.createElement("P");
                        text = document.createTextNode("* Password must contain at least one symbol character.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);

                        paragraph = document.createElement("P");
                        text = document.createTextNode("* Password must contain at least one numerical character.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);

                        paragraph = document.createElement("P");
                        text = document.createTextNode("* Password must contain at least 8 characters.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);

                        break;
                        
                    case 2:
                        var paragraph = document.createElement("P");
                        var text = document.createTextNode("* Passwords do not match.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);
                        break;
                        
                    case 3:
                        var paragraph = document.createElement("P");
                        var text = document.createTextNode("* Given name must contain Upper case and lower case characters.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);
                        break;
                        
                    case 4:
                        var paragraph = document.createElement("P");
                        var text = document.createTextNode("* Surname must contain Upper case and lower case characters.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);
                        break;
                        
                    case 5:
                        var paragraph = document.createElement("P");
                        var text = document.createTextNode("* Please enter a valid email.");
                        paragraph.appendChild(text);
                        document.getElementById("signup-validation-info").appendChild(paragraph);
                        break;
                }
            }
        }
        s_form = document.getElementById("signup-form");
        s_form.style.display = "none";
        modal = document.getElementById("signup-validation-modal");
        modal.style.display = "block";
    }

    return is_valid;
}

// check whether or not the year is the leap year.
// if it is the leap year, this function will return true.
// if it is not, this function will return false;
function check_leapyear(year){
    var leapyear = false;
    if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
        leapyear = true;
    }
    return leapyear;
}

// cancel-button functionality
function signup_cancel() {
    document.getElementById("uname-field").style.borderColor = "#666666";
    document.getElementById("passwd1-field").style.borderColor = "#666666";
    document.getElementById("passwd2-field").style.borderColor = "#666666";
    document.getElementById("fname-field").style.borderColor = "#666666";
    document.getElementById("lname-field").style.borderColor = "#666666";
    document.getElementById("email-field").style.borderColor = "#666666";
    document.getElementById("c_box").style.outline = '1px solid #666666';
}

// Terms and Conditions modal functionality
function terms_function() {
    s_form = document.getElementById("signup-form");
    s_form.style.display = "none";
    modal = document.getElementById('terms_conditions-modal');
    modal.style.display = "block";

}

// Terms and Conditions modal close functionality
function terms_close() {
    modal = document.getElementById('terms_conditions-modal');
    modal.style.display = "none";
    s_form = document.getElementById("signup-form");
    s_form.style.display = "block";
}

// Signup validation modal close functionality
function signup_validation_close() {
    modal = document.getElementById('signup-validation-modal');
    modal.style.display = "none";
    s_form = document.getElementById("signup-form");
    s_form.style.display = "block";
}

function off_alert(this_field) {
    this_field.style.borderColor = "#666666";
}

////////////////////////////
/* Login page javascrpit  */
////////////////////////////

function login_submit() {
    var is_valid = true;
    var username = document.forms["log-form"]["username"].value;
    var PASSWORD = document.forms["log-form"]["password"].value;
    document.getElementById('login-validation-info').innerHTML = 'Please correct the following errors:';

    if (username == ""){
        var paragraph = document.createElement("P");
        var text = document.createTextNode("* You must enter a username.");
        paragraph.appendChild(text);
        document.getElementById("login-validation-info").appendChild(paragraph);
        document.getElementById("user-field").style.borderColor = "red";
        is_valid = false;
    }

    if (PASSWORD == ""){
        var paragraph = document.createElement("P");
        var text = document.createTextNode("* You must enter a password.");
        paragraph.appendChild(text);
        document.getElementById("login-validation-info").appendChild(paragraph);
        document.getElementById("pw-field").style.borderColor = "red";
        is_valid = false;
    }

    s_form = document.getElementById("login-form");
    s_form.style.display = "none";
    modal = document.getElementById("login-validation-modal");
    modal.style.display = "block";

    return is_valid;
}

// Login validation modal close functionality
function login_validation_close() {
    modal = document.getElementById('login-validation-modal');
    modal.style.display = "none";
    s_form = document.getElementById("login-form");
    s_form.style.display = "block";
}

// cancel-button functionality
function login_cancel() {
    document.getElementById("pw-field").style.borderColor = "#666666";
    document.getElementById("user-field").style.borderColor = "#666666";
}

////////////////////////////////////
/* Search_Results page javascrpit */
////////////////////////////////////
/*
function initResultsMap() {
    var points = [
        ['Park1', -27.48076499, 153.0387956],
        ['Park2', -27.47578578, 153.0300306],
        ['Park3', -27.48194089, 153.0264731]
    ];
    //var points = getParkData();
    var infoWindow;
    var map = new google.maps.Map(document.getElementById('results-map'));
    infoWindow = new google.maps.InfoWindow;


    var bounds = new google.maps.LatLngBounds();

    for (index = 0; index < points.length; index++) {
        point = new google.maps.LatLng(points[index][1], points[index][2]);
        bounds.extend(point);
        marker = new google.maps.Marker({
            position: point,
            map: map,
            label: {
                fontWeight: 'bold',
                text: points[index][0]
            }
        });

        google.maps.event.addListener(marker, 'click', function() {
            window.location.href = 'review.php';
        });
    }

    map.fitBounds(bounds);

    // Find Current Location
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var myPos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(myPos);
            //map.setZoom(15);

            // Add users position marker
            userMarker = new google.maps.Marker({
                position: new google.maps.LatLng(myPos.lat, myPos.lng),
                map: map
            });
            userMarker.setIcon('images/green-dot.png');

        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
            });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }



    function getParkData() {


    }
}
*/
////////////////////////////
/* Review page javascrpit */
////////////////////////////

function initReviewMap() {

    var bot_lat = -27.47578578;
    var bot_long = 153.0300306;
    var botanic = {lat: bot_lat, lng: bot_long};

    var map = new google.maps.Map(document.getElementById('review-map'), {
        zoom: 16,
        center: botanic
    });
    
    var marker1 = new google.maps.Marker({
        position: botanic,
        map: map
    });        
}

/////////////////////////
/* Pass array function */
/////////////////////////

function passArray(array) {
    return array;
}