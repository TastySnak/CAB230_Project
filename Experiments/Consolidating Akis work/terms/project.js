// declare variables
var form, modal, auto_btn, sub_btn, park_btn, rate_btn;

///////////////////////////
/* Index page javascript */
///////////////////////////

// Auto-find functionality
auto_btn = document.getElementById("auto-button");

// When user clicks 'Closest to you' it goes to the closest park
auto_btn.onclick = function() {
    location.href="review.html";
}

// name-confirm-button functionality
n_conf_btn = document.getElementById("name-confirm-button");
n_conf_btn.onclick = function() {
    location.href="review.html";
}

// name-cancel-button functionality
n_can_btn = document.getElementById("name-cancel-button");
n_can_btn.onclick = function() {
    modal = document.getElementById('modal-p');
    document.getElementById("input-search").value="";
    modal.style.display = "none";
    form.style.display = "block";
}

// rating-confirm-button functionality
r_conf_btn = document.getElementById("rating-confirm-button");
r_conf_btn.onclick = function() {
    location.href="search_results.html";
}

// rating-cancel-button functionality
r_can_btn = document.getElementById("rating-cancel-button");
r_can_btn.onclick = function() {
    modal = document.getElementById('modal-r');
    modal.style.display = "none";
    form.style.display = "block";
}

// Get the button that opens the modal
form = document.getElementById("searchform");
sub_btn = document.getElementById("suburb-button");
park_btn = document.getElementById("park-button");
rate_btn = document.getElementById("rating-button");

// When the user clicks the a button, open the modal 
sub_btn.onclick = function() {
    modal = document.getElementById('modal-s');
    modal.style.display = "block";
    form.style.display = "none";
}

park_btn.onclick = function() {
    modal = document.getElementById('modal-p');
    modal.style.display = "block";
    form.style.display = "none";
}

rate_btn.onclick = function() {
    modal = document.getElementById('modal-r');
    modal.style.display = "block";
    form.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        form.style.display = "block";
        document.getElementById("input-search").value="";
    }
}

////////////////////////////
/* Signup page javascrpit */
////////////////////////////

// submit-button functionality
//s_conf_btn = document.getElementById("signup-confirm-button");
//s_conf_btn.onclick = function() {
function signup_submit() {
    var USERNAME = document.forms["reg-form"]["username"].value;
    var PASSWORD1 = document.forms["reg-form"]["password1"].value;
    var PASSWORD2 = document.forms["reg-form"]["password2"].value;
    var GIVENNAME = document.forms["reg-form"]["given"].value;
    var SURNAME = document.forms["reg-form"]["last"].value;
    var EMAIL = document.forms["reg-form"]["email"].value;
  
    // Upper cases
    var UPPER = /[A-Z]/;
    // Lower cases
    var LOWER = /[a-z]/;
    // Numbers
    var NUMRIC = /[0-9]/;
    // Symbols
    var SYMBOL = /[\W]/;
    // Space
    var SPACE = /\s/;
    // The minimum length of Passwords.
    var LENGTH = 8;
    // Emails validation.
    var EmailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var CHECKBOX = document.forms["reg-form"]["check"].checked;
    var DAY = document.forms["reg-form"]["day"].value;
    var MONTH = document.forms["reg-form"]["month"].value;
    var YEAR = document.forms["reg-form"]["year"].value;
    var JUDGEMENT = judgement(YEAR);
    // This section checks USERNAME, PASSWORD1, PASSWORD2, GIVENNAME, SURNAME, and EMAIL values if they are empty.
    // (Patterns = 6C1 + 6C2 + 6C3 + 6C4 + 6C5)
    // The searching borderColor makes easy to find every pattern.
    if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        alert("Please fill out your Username and Passwords and GivenName and Surname and Email.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;

    } else if ((PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Passwords and GivenName and Surname and Email.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Username and first Password and GivenName and Surname and Email.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Username and GivenName and Surname and Email, and confirm the Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Username and Passwords and Surname and Email.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Username and Passwords and GivenName and Email.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please fill out your Username and Passwords and GivenName and Surname.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please confirm your Password and fill out your GivenName and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false
    } else if ((PASSWORD1 == "") && (GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Password and GivenName and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD1 == "") && (PASSWORD2 == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Passwords and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Passwords and GivenName and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please fill out your Passwords and GivenName and Surname.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and GivenName and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Surname and Email address, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and GivenName and Email address, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please fill out UserName and GivenName and Surname, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Password and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Password and GivenName and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please fill out your UserName and Password and GivenName and Surname.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Passwords and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "") && (SURNAME == "")){
        window.alert("Please fill out your UseName and Passwords and Surname.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "")){
        window.alert("Please fill out your UserName and Passwords and GivenName.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((GIVENNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your GivenName and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD2 == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please confirm your Password and fill out your Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD2 == "") && (GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please confirm your Password and fill out your GivenName and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD2 == "") && (GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please confirm your Password and fill out your GivenName and Surname.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD1 == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Password and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD1 == "") && (GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Password and GivenName and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD1 == "") && (GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please fill out your Password and GivenName and SurName.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD1 == "") && (PASSWORD2 == "") && (EMAIL == "")){
        window.alert("Please fill out Passwords and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD1 == "") && (PASSWORD2 == "") && (SURNAME == "")){
        window.alert("Please fill out your Passwords and Surname.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD1 == "") && (PASSWORD2 == "") && (GIVENNAME == "")){
        window.alert("Please fill out your Passwords and GivenName.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and GivenName and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please fill out your UserName and GivenName and Surname.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Email address, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "") && (SURNAME == "")){
        window.alert("Please fill out your UserName and Surname, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "") && (GIVENNAME == "")){
        window.alert("Please fill out your UserName and GivenName, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Password and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (SURNAME == "")){
        window.alert("Please fill out your UserName and Password and Surname.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (GIVENNAME == "")){
        window.alert("Please fill out your UserName and Passwords and GivenName.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "") && (PASSWORD2 == "")){
        window.alert("Please fill out your UserName and Passwords.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD1 == "")){
        window.alert("Please fill out your UserName and Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (PASSWORD2 == "")){
        window.alert("Please fill out your UserName, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (GIVENNAME == "")){
        window.alert("Please fill out your UserName and GivenName.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (SURNAME == "")){
        window.alert("Please fill out your UserName and Surname.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((USERNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your UserName and Email address.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD1 == "") && (PASSWORD2 == "")){
        window.alert("Please fill out your Passwords.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD1 == "") && (GIVENNAME == "")){
        window.alert("Please fill out your Password and GivenName.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD1 == "") && (SURNAME == "")){
        window.alert("Please fill out your Password and Surname.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD1 == "") && (EMAIL == "")){
        window.alert("Please fill out your Password and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((PASSWORD2 == "") && (GIVENNAME == "")){
        window.alert("Please confirm your Password and fill out your GivenName.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD2 == "") && (SURNAME == "")){
        window.alert("Please confirm your Password and fill out your Surname.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD2 == "") && (EMAIL == "")){
        window.alert("Please confirm your Password and fill out your Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((GIVENNAME == "") && (SURNAME == "")){
        window.alert("Please fill out your GivenName and Surname.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((GIVENNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your GivenName and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((SURNAME == "") && (EMAIL == "")){
        window.alert("Please fill out your Surname and Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if (USERNAME == ""){
        window.alert("Please fill out your Username.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (PASSWORD1 == ""){
        window.alert("Please fill out your Password.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (PASSWORD2 == ""){
        window.alert("Please confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (GIVENNAME == ""){
        window.alert("Please fill out your GivenName.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (SURNAME == ""){
        window.alert("Please fill out your Surname");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (EMAIL == ""){
        window.alert("Please fill out your Email address.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } 
    // This section checks USERNAME, PASSWORD1, PASSWORD2, GIVENNAME, SURNAME, and EMAIL values if they are invalid.
    // (Patterns = 6C1 + 6C2 + 6C3 + 6C4 + 6C5)
    // The searching borderColor makes easy to find every pattern.
    else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && ((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Passwords and GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1))))
    && ((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Passwords and GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and re-entered Password and GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Password and GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Passwords and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Passwrods and GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and Passwords and GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your re-entered Password and GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Password and GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1))))
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Passwords and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Passwords and GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your Passwords and GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Please fill out your UserName and Surname and Email address, and confirm your Password.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and re-entered Password and GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and re-entered Password and GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Password and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Password and GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and Password and GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Passwords and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2)))))
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and Passwords and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME)))))){
        window.alert("Your UserName and Passwords and GivenName are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your GivenName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your re-entered Password and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your re-entered Password and GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your re-entered Password and GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Password and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Password and GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your Password and GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("YOur Passwords and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your Passwords and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME)))))){
        window.alert("Your Passwords and GivenName are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and re-entered Password and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and re-entered Password and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME)))))){
        window.alert("Your UserName and re-entered Password and GivenName are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Password and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME)))))
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and Password and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME)))))){
        window.alert("Your UserName and Password and GivenName are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2)))))){
        window.alert("Your UserName and Passwords are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1))))){
        window.alert("Your UserName and Password are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2)))))){
        window.alert("Your UserName and re-entered Password are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME)))))){
        window.alert("Your UserName and GivenName are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your UserName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your UserName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2)))))){
        window.alert("Your Passwords are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME)))))){
        window.alert("Your Password and GivenName are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your Password and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Password and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2)))))
    && (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME)))))){
        window.alert("Your re-entered Password and GivenName are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your re-entered Password and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your re-entered Password and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME)))))){
        window.alert("Your GivenName and Surname are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your GivenName and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if ((((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))) 
    && ((!(EmailValidation.test(EMAIL))))){
        window.alert("Your Surname and Email address are invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } else if (((!(UPPER.test(USERNAME))) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))){
        window.alert("Your UserName is invalid.");
        document.getElementById("uname-field").style.borderColor = "red";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (((PASSWORD1.length < LENGTH) || (!(UPPER.test(PASSWORD1))) || (!(LOWER.test(PASSWORD1))) || (!(NUMRIC.test(PASSWORD1))) || (SYMBOL.test(PASSWORD1)) || (SPACE.test(PASSWORD1)))){
        window.alert("Your Password is invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "red";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (((!(PASSWORD1 == PASSWORD2)) || ((PASSWORD2.length < LENGTH) || (!(UPPER.test(PASSWORD2))) || (!(LOWER.test(PASSWORD2))) || (!(NUMRIC.test(PASSWORD2))) || (SYMBOL.test(PASSWORD2)) || (SPACE.test(PASSWORD2))))){
        window.alert("Your re-entered Password is invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "red";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (((!(UPPER.test(GIVENNAME))) || (!(LOWER.test(GIVENNAME))))){
        window.alert("Your GivenName is invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "red";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if (((!(UPPER.test(SURNAME))) || (!(LOWER.test(SURNAME))))){
        window.alert("Your Surname is invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "red";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else if ((!(EmailValidation.test(EMAIL)))){
        window.alert("Your Email address is invalid.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "red";
        return false;
    } 
    // Validate the Date of Birth
    // February, depending on whether or not the year is the leap year.
    else if ((MONTH == 2) && (JUDGEMENT == false) && (DAY > 28)){
        window.alert("test1.");
        return false;
    } else if ((MONTH == 2) && (JUDGEMENT == true) && (DAY > 29)){
        window.alert("test2.");
        return false;
    } else if ((MONTH == 4) && (DAY == 31)){
        window.alert("Your Date of Birth is invalid.");
        document.getElementById("day").style.borderColor = "red";
        return false;
    } else if ((MONTH == 6) && (DAY == 31)){
        window.alert("Your Date of Birth is invalid.");
        return false;
    } else if ((MONTH == 9) && (DAY == 31)){
        window.alert("Your Date of Birth is invalid.");
        return false;
    } else if ((MONTH == 11) && (DAY == 31)){
        window.alert("Your Date of Birth is invalid.");
        return false;
    } 
    // Check whether or not the checkbox is checked.
    else if (CHECKBOX == false){
        window.alert("Please accept terms and conditions.");
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        return false;
    } else {
        location.href="index.html";
        document.getElementById("uname-field").style.borderColor = "#666666";
        document.getElementById("passwd1-field").style.borderColor = "#666666";
        document.getElementById("passwd2-field").style.borderColor = "#666666";
        document.getElementById("fname-field").style.borderColor = "#666666";
        document.getElementById("lname-field").style.borderColor = "#666666";
        document.getElementById("email-field").style.borderColor = "#666666";
        document.getElementById("uname-field").value="";
        document.getElementById("passwd1-field").value="";
        document.getElementById("passwd2-field").value="";
        document.getElementById("fname-field").value="";
        document.getElementById("lname-field").value="";
        document.getElementById("email-field").value="";
        return true;
    } 
}

// check whether or not the year is the leap year.
// if it is the leap year, this function will return true.
// if it is not, this function will return false;
function judgement(YEAR){
    var JUDGEMENT = false;
    if ((YEAR % 400 == 0) || (YEAR % 4 == 0 && YEAR % 100 != 0)){
        JUDGEMENT = true;
    }
    return JUDGEMENT;
}

// cancel-button functionality
function signup_cancel() {
    document.getElementById("uname-field").style.borderColor = "#666666";
    document.getElementById("passwd1-field").style.borderColor = "#666666";
    document.getElementById("passwd2-field").style.borderColor = "#666666";
    document.getElementById("fname-field").style.borderColor = "#666666";
    document.getElementById("lname-field").style.borderColor = "#666666";
    document.getElementById("email-field").style.borderColor = "#666666";
}

// Terms and Conditions modal functionality
function terms_function() {
    s_form = document.getElementById("signup-form");
    s_form.style.display = "none";
    modal = document.getElementById('terms_conditions-modal');
    modal.style.display = "block";

}

// Terms and Conditions modal close functionality
function terms_close() {
    modal = document.getElementById('terms_conditions-modal');
    modal.style.display = "none";
    s_form = document.getElementById("signup-form");
    s_form.style.display = "block";
}

////////////////////////////
/* Login page javascrpit  */
////////////////////////////

// submit-button functionality
// l_conf_btn = document.getElementById("login-confirm-button");
// l_conf_btn.onclick = function() {
function login_submit() {
    var USERNAME = document.forms["log-form"]["username"].value;
    var PASSWORD = document.forms["log-form"]["password"].value;
    var UPPER = /[A-Z]/;
    var LOWER = /[a-z]/;
    var NUMRIC = /[0-9]/;
    var SYMBOL = /[\W]/;
    var SPACE = /\s/;
    var LENGTH = 8;
    if ((USERNAME == "") && (PASSWORD == "")){
        window.alert("Please fill out your Username and Password.");
        document.getElementById("user-field").style.borderColor = "red";
        document.getElementById("pw-field").style.borderColor = "red";
        return false;
    } else if (USERNAME == ""){
        window.alert("Please fill out your Username.");
        document.getElementById("user-field").style.borderColor = "red";
        document.getElementById("pw-field").style.borderColor = "#666666";
        return false;
    } else if (PASSWORD == ""){
        window.alert("Please fill out your Password.");
        document.getElementById("pw-field").style.borderColor = "red";
        document.getElementById("user-field").style.borderColor = "#666666";
        return false;
    } else if ((!(UPPER.test(USERNAME)) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME)))) 
    && ((PASSWORD.length < LENGTH) || (!(UPPER.test(PASSWORD))) || (!(LOWER.test(PASSWORD))) || (!(NUMRIC.test(PASSWORD))) || (SYMBOL.test(PASSWORD)) || (SPACE.test(PASSWORD)))){
        window.alert("Your UserName and Password are invalid. Please try again.\n\nHint:\nA UserName must include Upper and Lower cases and numbers.\n\nHint:\nA Password's length must be more than 8 and A Password must include Upper and Lower cases and numbers.");
        document.getElementById("user-field").style.borderColor = "red";
        document.getElementById("pw-field").style.borderColor = "red";
        return false;
    } else if ((!(UPPER.test(USERNAME)) || (!(LOWER.test(USERNAME))) || (!(NUMRIC.test(USERNAME))))){
        window.alert("Your Username is invalid. Please try again.\n\nHint:\nA UserName must include Upper and Lower cases");
        document.getElementById("user-field").style.borderColor = "red";
        document.getElementById("pw-field").style.borderColor = "#666666";
        return false;
    } else if ((PASSWORD.length < LENGTH) || (!(UPPER.test(PASSWORD))) || (!(LOWER.test(PASSWORD))) || (!(NUMRIC.test(PASSWORD))) || (SYMBOL.test(PASSWORD)) || (SPACE.test(PASSWORD))){
        window.alert("Your Password is invalid. Please try again.\n\nHint:A Password's length must be more than 8 and A Password must include Upper and Lower cases and numbers.")
        document.getElementById("pw-field").style.borderColor = "red";
        document.getElementById("user-field").style.borderColor = "#666666";
        return false;
    } else {
        location.href="index.html";
        document.getElementById("pw-field").style.borderColor = "#666666";
        document.getElementById("user-field").style.borderColor = "#666666";
        document.getElementById("user-field").value="";
        document.getElementById("pw-field").value="";
        return true;
    } 
}

// cancel-button functionality
function login_cancel() {
    document.getElementById("pw-field").style.borderColor = "#666666";
    document.getElementById("user-field").style.borderColor = "#666666";
}

////////////////////////////////////
/* Search_Results page javascrpit */
////////////////////////////////////

function initResultsMap() {
    var ray_lat = -27.48076499;
    var ray_long = 153.0387956;
    var ray = {lat: ray_lat, lng: ray_long};

    var bot_lat = -27.47578578;
    var bot_long = 153.0300306;
    var botanic = {lat: bot_lat, lng: bot_long};

    var mar_lat = -27.48194089;
    var mar_long = 153.0264731;
    var mar = {lat: mar_lat, lng: mar_long};

    var map = new google.maps.Map(document.getElementById('results-map'), {
        zoom: 15,
        center: botanic
    });

    var marker1 = new google.maps.Marker({
        position: ray,
        map: map
    });
    
    var marker2 = new google.maps.Marker({
        position: botanic,
        map: map
    });

    var marker3 = new google.maps.Marker({
        position: mar,
        map: map
    });
}

////////////////////////////
/* Review page javascrpit */
////////////////////////////

function initReviewMap() {

    var bot_lat = -27.47578578;
    var bot_long = 153.0300306;
    var botanic = {lat: bot_lat, lng: bot_long};

    var map = new google.maps.Map(document.getElementById('review-map'), {
        zoom: 16,
        center: botanic
    });
    
    var marker1 = new google.maps.Marker({
        position: botanic,
        map: map
    });        
}