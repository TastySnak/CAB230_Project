<!DOCTYPE html>
<?php  
    session_start();
    include 'pdo.php';
/*
    // Checks to see if someone is already logged in and redirects to home page if true
    if (isset($_SESSION['user'])) {
        header("Location:index.php");
    }

    // Checks to see if the login submit button was clicked
    if (isset($_POST['login'])) {
        $username = $_POST['UserName'];
        $password = $_POST['PassWord'];

        try {
            $userQuery = $pdo->query('SELECT userName, password FROM users WHERE userName == ', $username);
            if ($userQuery->rowCount() > 0) {
                if ($password == 'password') {
                    $_SESSION['user'] = $username;
             
                    // <script type="text/javascript">window.location.href = 'index.php';</script>
                } else {

                    // <script type="text/javascript">window.location.href = 'invalid_password.php';</script>

                }
            } else {      

                // <script type="text/javascript">window.location.href = 'invalid_username.php';</script>
             
            }
            
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }*/
?>

<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Log in</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>
    <body id="body-login"> 
        
        <div class="wrapper">

            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    <div id="menu">
                        <a class="menu" href="signup.php">sign up</a>
                        <a class="menu" href="login.php">log in</a>
                    </div>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Content -->
            <div class="content-form">
                <?php
                    include 'login_form.php';
                ?>

                <!-- Validation error modal -->
                <div id="login-validation-modal" class="modal">
                    <a id="login-validation-close-button" href="javascript:login_validation_close();">Close</a>
                    <div id="login-validation-info"></div>
                </div>
            </div>

            <!-- Footer -->
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>

        </div>
    </body>
</html>