<?php
    echo "<form id=\"login-form\" name=\"log-form\">";
        include 'functions.php';

        field_id_label('login-title', 'Log in');

        field_class_label('field-label', 'Username');
        input_field('user-field', 'input-login', 'text', 'UserName');
        
        field_class_label('field-label', 'Password');
        input_field('pw-field', 'input-login', 'password', 'PassWord');

        form_button('submit', 'Submit', 'login-confirm-button', 'confirm-button', 'login', 'return login_submit();');
        form_button('reset', 'Cancel', 'login-cancel-button', 'cancel-button', 'login-cancel', 'login_cancel();');

        echo "<div id=\"register\"><h1>New users</h1><h2> If you don't have your account, please <a href=\"signup.php\">register</a>.</h2></div>";

    echo "</form>";
?>