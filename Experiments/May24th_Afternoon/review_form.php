<?php
    echo "<form id=\"user-review-form\" name=\"rev-form\" action=\"review-form.php\" method=\"POST\">";
        include 'functions.php';

        field_id_label('regist-title', 'Write a review');

        field_label('Review Title');
        review_title_input_field();

        field_label('Review Rating');
        include 'review_buttons.php';

        field_label('Review Content');
        review_content_input_field();

        form_button('submit', 'Submit', 'review-confirm-button', 'confirm-button', 'review-submit', 'return signup_submit();');
        form_button('reset', 'Cancel', 'review-cancel-button', 'cancel-button', 'review-cancel', 'signup_cancel();');    
    echo "</form>";
?>