<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();
    header("Refresh: 2; URL=http://{$_SERVER['HTTP_HOST']}/index.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Signup</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>

    <body id="body-signup"> 
        
        <div class="wrapper">

            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Content -->
            <div class="content-form">
                <!-- Message Box -->
                <div id="message-box">
                    <div class="field-label">You are still signed in!</div><br>
                    <div class="field-label">Please log out first to perform this action.</div>
                </div>
            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>