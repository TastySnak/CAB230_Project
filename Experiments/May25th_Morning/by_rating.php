<!DOCTYPE html>
<?php 
    session_start(); 
    include 'pdo.php';
?>

<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Home</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">

        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Rating Form -->

            <div id="modal-r" class="searchArea">
                <!-- Modal content -->
                <div class="modal-content-r">
                    <form class="searchbox" method="post">
                        <div class="searchRating">Choose minimum rating:</div>
                        <?php
                            include 'rate_buttons.php';
                        ?>
                        <input type="submit" value="Confirm" id="rating-confirm-button" class="confirm-button">
                        <input type="cancel" value="Cancel" id="rating-cancel-button" class="cancel-button" onclick="window.location='index.php'">
                    </form>
                </div>
            </div>
            
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>