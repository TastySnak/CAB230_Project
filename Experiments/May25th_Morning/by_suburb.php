<!DOCTYPE html>
<?php 
    session_start(); 
    include 'pdo.php';
?>

<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Suburb Search</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">

        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Suburb Form -->

            <div id="modal-s" class="searchArea">
                <!-- Modal content -->
                <div class="modal-content-s">
                    <div class="searchSuburb">Suburb List</div>
                    <div class="dropdown-content">
                        <?php
                            $suburbQuery = $pdo->query('SELECT DISTINCT Suburb FROM parks ORDER BY Suburb');
                            foreach ($suburbQuery as $suburbName) {
                                $suburb = $suburbName["Suburb"];
                                echo '<input type="submit" name="suburb", value="',$suburb,'" onclick="window.location=\'search_results.php?suburb=',$suburb,'\'" />';
                            }
                        ?>
                    </div>
                    <input type="cancel" value="Cancel" id="suburb-cancel-button" onclick="window.location='index.php'">   
                </div>   
            </div>

           
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>