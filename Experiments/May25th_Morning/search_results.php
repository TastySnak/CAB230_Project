<!DOCTYPE html>
<?php
    session_start();
    include 'pdo.php';

    // Global array to hold coords of each location
    $GLOBALS['location'] = array();
?>
<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Results</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
        <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpfbaVQFDHZJZPwtT_PLCHa63xB6Gg2JM&callback=initResultsMap"></script>

    </head>
    <body id="body-results">       
        <div class="wrapper">
            
            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Content -->
            <div class="inner-content">
                
                <div id="results-droplist">
                    <div id="search-title" class="title">Search Results</div>
                    <div class="results-dropdown-content">
                        <?php
                            $sub_name = $_GET['suburb'];
                            try {
                                $nameQuery = $pdo->query("SELECT Name, Latitude, Longitude FROM parks WHERE Suburb = '$sub_name'");
                                foreach ($nameQuery as $parkName) {
                                    $park = $parkName["Name"];
                                    array_push($GLOBALS['location'], {lat: $parkName["Latitude"], lng: $parkName["Longitude"]});
                                    echo '<input type="submit" name="',$park,'", value="',$park,'" onclick="window.location=\'review.php\'" />';                                    
                                }
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                            }
                        ?>
                    </div>
                </div>

                <div id="results-map-border">
                    <div id="results-map"></div>
                </div>
            </div>

            <!-- Footer -->
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
        <script>
            function initResultsMap() {
                var infoWindow;
                var map = new google.maps.Map(document.getElementById('results-map'));
                infoWindow = new google.maps.InfoWindow;


                var bounds = new google.maps.LatLngBounds();

                for (index = 0; index < $GLOBALS['location'].length; index++) {
                    point = new google.maps.LatLng($GLOBALS['location'][index][1], $GLOBALS['location'][index][2]);
                    bounds.extend(point);
                    marker = new google.maps.Marker({
                        position: point,
                        map: map,
                        label: {
                            fontWeight: 'bold',
                            text: $GLOBALS['location'][index][0]
                        }
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        window.location.href = 'review.php';
                    });
                }

                map.fitBounds(bounds);

                // Find Current Location
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var myPos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        map.setCenter(myPos);
                        //map.setZoom(15);

                        // Add users position marker
                        userMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(myPos.lat, myPos.lng),
                            map: map
                        });
                        userMarker.setIcon('images/green-dot.png');

                    }, function() {
                        handleLocationError(true, infoWindow, map.getCenter());
                        });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }
            }
        </script>
    </body>
</html>