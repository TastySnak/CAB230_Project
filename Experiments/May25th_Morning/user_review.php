<!DOCTYPE html>
<?php
    session_start();
    include 'pdo.php';
    if (!isset($_SESSION['user'])) {
        header("Location: http://{$_SERVER['HTTP_HOST']}/index.php");
        exit();
    }
?>
<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - User Review Page</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>

    <body id="body-user-rev"> 
        
        <div class="wrapper">

            <!-- Header -->
            <?php
                include 'header-logged-in.php';
            ?>

            <!-- Content -->
            <div class="review-content-area">
                <form id="user-review-form" name="rev-form" action="review-form.php" method="POST">

                    <div id="regist-title">Write a review</div>

                    <div class="field-label">Review Title</div>
                    <div><input id="review-title-field" type="text" name="rev-title" placeholder="Enter title here..." onkeypress="off_alert(this)" required>

                    <div class="field-label">Review Rating</div>
                    <div id="review-buttons" class="rating_choice">
                        <input id="rating5" type="radio" name="rating" value="5">
                        <label for="rating5"></label>
                        <input id="rating4" type="radio" name="rating" value="4">
                        <label for="rating4"></label>
                        <input id="rating3" type="radio" name="rating" value="3" checked>
                        <label for="rating3"></label>
                        <input id="rating2" type="radio" name="rating" value="2">
                        <label for="rating2"></label>
                        <input id="rating1" type="radio" name="rating" value="1">
                        <label for="rating1"></label>
                    </div>

                    <div class="field-label">Review Content</div>
                    <div><textarea rows="10" cols="50" id="review-content-field" type="text" name="rev-content" placeholder="Enter review here..." onkeypress="off_alert(this)" required></textarea></div>

                    <input type="submit" value="Submit" id="review-confirm-button" class="confirm-button" name="review-submit" onclick="return signup_submit();">
                    <input type="reset" value="Cancel" id="review-cancel-button" class="cancel-button" name="review-cancel" onclick="signup_cancel();">
                    
                </form>

            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>