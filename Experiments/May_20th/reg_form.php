<?php
    echo "<form id=\"signup-form\" name=\"reg-form\" action=\"signup1.php\" method=\"POST\">";
        include 'functions.inc';

        field_id_label('regist-title', 'Create an account');

        field_label('Username');
        input_field('uname-field', 'input-signup', 'text', 'username');

        field_label('Password');
        input_field('passwd1-field', 'input-signup', 'password', 'password1');

        field_label('Re-enter Password');
        input_field('passwd2-field', 'input-signup', 'password', 'password2');

        field_id_label('name-label1', 'Given name');
        field_id_label('name-label2', 'Surname');
        
        reg_name_input_field('fname-field', 'given');
        reg_name_input_field('lname-field', 'last');
        
        field_label('Date of Birth');
        DOB_dropdowns($months);
        field_label('Email Address');

        echo "<div><input id=\"email-field\" class=\"input-signup\" type=\"text\" name=\"email\" placeholder=\"email@address.com\" onkeypress=\"off_alert(this)\"></div>";
        echo "<div id=\"terms\">By creating an account, you must accept<br>Brisbane Park Finder's <a id=\"tnc\" href=\"javascript:terms_function();\">terms and conditions.</a><input id=\"c_box\" type=\"checkbox\" name=\"check\" onclick=\"off_alert(this)\"></div>";

        form_button('submit', 'Submit', 'signup-confirm-button', 'confirm-button', 'return signup_submit();');
        form_button('reset', 'Cancel', 'signup-cancel-button', 'cancel-button', 'signup_cancel();');    
    echo "</form>";
?>