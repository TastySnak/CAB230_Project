<!DOCTYPE html>
<?php 
    session_start(); 
    include 'pdo.inc';
?>

<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Suburb Search</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">

        <div class="wrapper">

            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    <div id="header_menu">
                        <a class="menu" href="signup.php">sign up</a>
                        <a class="menu" href="login.php">log in</a>
                    </div>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Suburb Form -->

            <div id="modal-s" class="modal">
                <!-- Modal content -->
                <div class="modal-content-s">
                    <div class="searchbox">
                        <div class="searchSuburb">Suburb List</div>

                        <div class="dropdown-content">
                            <?php
                                $suburbQuery = $pdo->query('SELECT DISTINCT Suburb FROM parks ORDER BY Suburb');
                                foreach ($suburbQuery as $suburbName) {
                                    $suburb = $suburbName["Suburb"];
                                    echo '<input type="submit" name="suburb", value="',$suburb,'" onclick="window.location=\'search_results.php?suburb=',$suburb,'\'" />';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

           
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>