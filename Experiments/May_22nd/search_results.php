<!DOCTYPE html>
<?php
    session_start();
    include 'pdo.inc';
?>
<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Results</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
        <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpfbaVQFDHZJZPwtT_PLCHa63xB6Gg2JM&callback=initResultsMap"></script>

    </head>
    <body id="body-results">       
        <div class="wrapper">
            
            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    <div id="menu">
                        <a class="menu" href="signup.php">sign up</a>
                        <a class="menu" href="login.php">log in</a>
                    </div>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Content -->
            <div class="inner-content">
                <div id="results-map-border">
                    <div id="results-map"></div>
                </div>
                <div id="results-droplist">
                    <div id="search-title" class="title">Search Results</div>
                    <div class="results-dropdown-content">
                        <?php
                            $sub_name = $_GET['suburb'];
                            try {
                                $parkQuery = $pdo->query("SELECT * FROM parks WHERE Suburb = '$sub_name'");
                                foreach ($parkQuery as $parkName) {
                                    $park = $parkName["Name"];
                                    $_SESSION['park_name'] = $parkName["Name"];
                                    echo '<input type="submit" name="',$park,'", value="',$park,'" onclick="window.location=\'review.php\'" />';                                    
                                }
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                            }
                        ?>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>

    </body>
</html>