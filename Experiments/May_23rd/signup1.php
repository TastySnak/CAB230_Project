<?php
	include 'pdo.inc';
	if ($_POST){
		try {				
			$stmt = $pdo->prepare('INSERT INTO users (userName, givenName, lastName, day, month, year, emailAddress, password) VALUES (:userName, :givenName, :lastName, :day, :month, :year, :emailAddress, :password)');
			
			$hash = password_hash($_POST['password1'], PASSWORD_DEFAULT);
			
			$stmt->bindValue(':userName', $_POST['username']);
			$stmt->bindValue(':givenName', $_POST['given']);
			$stmt->bindValue(':lastName', $_POST['last']);
			$stmt->bindValue(':day', $_POST['day']);
			$stmt->bindValue(':month', $_POST['month']);
            $stmt->bindValue(':year', $_POST['year']);
            $stmt->bindValue(':emailAddress', $_POST['email']);
            $stmt->bindValue(':password', $hash);

			$stmt->execute();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}					
	}

?>