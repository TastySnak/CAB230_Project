<?php
	include 'pdo.inc';
	if ($_POST){
		try {				
			$rev = $pdo->prepare('INSERT INTO reviews (reviewTitle, reviewText) VALUES (:rev-title, :rev-content');
			
			$rev->bindValue(':rev-title', $_POST['rev-title']);
			$rev->bindValue(':rev-content', $_POST['rev-content']);

			$rev->execute();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}					
	}

?>