<?php
    $months = array(
        1 => 'January', 
        2 => 'February', 
        3 => 'March', 
        4 =>'April', 
        5 => 'May', 
        6 => 'June', 
        7 => 'July', 
        8 => 'August', 
        9 => 'September', 
        10 => 'October', 
        11 => 'November', 
        12 => 'December'
    );

    function input_field($field_id, $field_class, $field_type, $field_name) {
        echo "<div><input id=\"$field_id\" class=\"$field_class\" type=\"$field_type\" name=\"$field_name\" onkeypress=\"off_alert(this)\"></div>";
    }

    function reg_name_input_field($field_id, $field_name) {
        echo "<div class=\"name-field\"><input id=\"$field_id\" class=\"n-field\" type=\"text\" name=\"$field_name\" onkeypress=\"off_alert(this)\"></div>";
    }

    function field_label($label) {
        echo "<div class=\"field-label\">$label</div>";
    }

    function field_id_label($id, $label) {
        echo "<div id=\"$id\">$label</div>";
    }

    function field_class_label($class, $label) {
        echo "<div class=\"$class\">$label</div>";
    }

    function DOB_dropdowns($months) {
        echo "<div id=\"date-fields\">";
        echo "<div id=\"days\">";
        DOB_days();
        echo "<div class=\"separator\">/</div></div>";
        echo "<div id=\"months\">";
        DOB_months($months);
        echo "<div class=\"separator\">/</div></div>";
        echo "<div id=\"years\">";
        DOB_years();
        echo "</div></div>";
    }

    function DOB_days() {
        echo "<select name=\"day\">";
        for ($day=1; $day<32; $day++) {
            echo "<option value=\"$day\">$day</option>";
        }
        echo "</select>";
    }

    function DOB_months($months) {
        echo "<select name=\"month\">";
        foreach ($months as $month_value => $month_text) {
            echo "<option value=\"$month_value\">$month_text</option>";
        }
        echo "</select>";
    }

    function DOB_years() {
        echo "<select name=\"year\" autocomplete=\"off\">";
        for ($year=1918; $year<2018; $year++) {
            if ($year == 2000) {
                echo "<option value=\"$year\" selected>$year</option>";
            } else {
                echo "<option value=\"$year\">$year</option>";
            }
        }
        echo "</select>";
    }

    function form_button($type, $value, $id, $class, $onclick) {
        echo "<input type=\"$type\" value=\"$value\" id=\"$id\" class=\"$class\" onclick=\"$onclick\">";
    }
?>