<!DOCTYPE html>
<html>
    <head>
        <!-- 
            CAB230 Project Authors:
            Gary Murphy   n9408410
            Aki Maruyama
        -->
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Signup</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>

    <body id="body-signup"> 
        
        <div class="wrapper">

            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    <div id="menu">
                        <a class="menu" href="signup.php">sign up</a>
                        <a class="menu" href="login.php">log in</a>
                    </div>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Content -->
            <div class="content-form">
                <?php
                    include 'reg_form.php';
                ?>

                <!-- Term and conditions modal -->
                <div id="terms_conditions-modal" class="modal">
                    <div id="terms_background">
                        <p id="terms-form-title">Terms and Conditions</p>
                        <p>Maecenas odio dolor, suscipit vel faucibus eu, rhoncus quis libero. Sed fermentum et ex vel tempus. Cras nec ultricies nunc, non mollis quam. Morbi nec neque nec ex placerat hendrerit. Vestibulum in facilisis lectus, sed faucibus eros. Aliquam ornare porttitor metus in pharetra. Praesent finibus quam vitae tincidunt laoreet. Curabitur in congue tortor. Mauris velit nisi, congue id lacinia ac, cursus non ligula. Nam sit amet nulla quis magna sagittis eleifend. Mauris vel suscipit dui. Integer efficitur libero sit amet porttitor luctus. Praesent sed blandit magna. Duis lobortis, ante non aliquam consequat, ante lectus imperdiet magna, nec sollicitudin est ipsum vitae nulla. In tincidunt ipsum a arcu vestibulum, in porta arcu dictum. </p>
                        <p>Morbi placerat nisi vitae dui placerat egestas bibendum vitae mauris. Phasellus id imperdiet nibh. Mauris sed dui euismod, auctor lorem nec, suscipit ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam pellentesque nulla ac velit rhoncus, nec ultrices quam pulvinar. Nullam eu lobortis magna, sit amet lacinia nisl. Sed elementum mollis neque, in facilisis turpis sodales in. In porttitor nibh convallis, luctus nibh in, ultrices nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque sed tristique libero. Nulla ipsum urna, pulvinar nec molestie at, pretium ut nisl. Aliquam pulvinar justo a congue scelerisque. Mauris vel metus quis erat porta gravida vel eget est. </p>
                        <p>Quisque vel tempus purus, rutrum fermentum nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed mollis massa eu accumsan rhoncus. Praesent egestas risus eget lorem varius accumsan. Aliquam erat volutpat. Donec sed erat scelerisque, molestie leo iaculis, vehicula quam. Pellentesque tincidunt fermentum risus et egestas. In ac varius orci. Nullam id sollicitudin urna, nec malesuada risus. Nam in tellus pretium, egestas enim at, suscipit nulla. Curabitur eget massa interdum, mollis lacus id, laoreet est. Morbi consectetur varius eros. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p><br>
                        <a id="terms-close-button" href="javascript:terms_close();">Close</a>
                    </div>
                </div>

                <!-- Validation error modal -->
                <div id="signup-validation-modal" class="modal">
                    <div id="signup-validation-info"></div>
                    <a id="signup-validation-close-button" href="javascript:signup_validation_close();">Close</a>
                </div>
            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>