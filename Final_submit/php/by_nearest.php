<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php 
    session_start(); 
    include 'pdo.php';
    include 'functions.php';

    // Hacky way to avoid *Undefined index notice on review page
    // hardcoded placeholder for actual working code. 

    // Couldn't figure out to do this without using AJAX or other frameworks that aren't permitted. =(
    // Would love to get feedback on how this is done.
    
    $park = "CITY-BOTANIC-GARDENS";

    header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/php/review.php?park=$park");
?>
