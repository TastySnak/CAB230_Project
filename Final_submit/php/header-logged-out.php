<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    echo '
    <div id="header">
        <div class="nograd">
            <a id="logo" href="../index.php">
                <img src="../images/logo.png" onerror="this.src=\'images/logo.png\'" alt="logo">
            </a>
            <div id="header_menu">
                <a class="menu" href="signup.php">sign up</a>
                <a class="menu" href="login.php">log in</a>
            </div>
            
        </div>
        <div id="h_grad"></div>
    </div>
    ';
?>