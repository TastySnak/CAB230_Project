<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php 
    session_start(); 
    include 'pdo.php';

    if (isset($_POST['rating-submit'])){
        $rating = $_POST['rating'];
        header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/search_results_rating.php?rating=$rating");
    }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Home</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>


    <body id="body-index">

        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Rating Form -->

            <div id="modal-r" class="searchArea">
                <!-- Modal content -->
                <div class="modal-content-r">
                    <form class="searchbox" method="post" action="by_rating.php">
                        <div class="searchRating">Choose minimum rating:</div>
                        <?php
                            echo '<div id="rating-buttons" class="rating_choice">
                                    <input id="rating5" type="radio" name="rating" value="5">
                                    <label for="rating5"></label>
                                    <input id="rating4" type="radio" name="rating" value="4">
                                    <label for="rating4"></label>
                                    <input id="rating3" type="radio" name="rating" value="3">
                                    <label for="rating3"></label>
                                    <input id="rating2" type="radio" name="rating" value="2" checked>
                                    <label for="rating2"></label>
                                    <input id="rating1" type="radio" name="rating" value="1">
                                    <label for="rating1"></label>
                                </div>';
                            echo '<input type="submit" value="Confirm" name="rating-submit" id="rating-confirm-button" class="confirm-button">';
                        ?>
                        <input type="cancel" value="Cancel" id="rating-cancel-button" class="cancel-button" onclick="window.location='index.php'">
                    </form>
                </div>
            </div>
            
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>