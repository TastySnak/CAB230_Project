<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start(); 
    include 'pdo.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Reviews</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script> 
        <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpfbaVQFDHZJZPwtT_PLCHa63xB6Gg2JM&callback=initReviewMap"></script>
    </head>
    <body id="body-review"> 
        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Content -->
            <div class="inner-content">
                <div id="info-box">                    
                    <div class="title">Park Information</div>
                    <div id= "park-details">City Botanical Gardens<br>Alice Street, Brisbane</div>
                    <div id="review-map"></div>
                </div>

                <div id="review-box">
                    <?php
                        if (isset($_SESSION['user'])) {            
                            echo '<div class="title">User Reviews</div>';
                            echo '<input type="submit" name="leave-review" value="Submit Review" id="leave-review-button" onclick="window.location=\'user_review.php\'" >';
                            echo '<div id="review-list-loggedin">';
                        } else {            
                            echo '<div class="title">User Reviews</div>';
                            echo '<div id="review-list-loggedout">';
                        }
                    ?>

                    <?php
                        // Get the list of reviews
                        try {
                            $park = $_GET['park'];                         
                            $parkName = str_replace('-',' ',$park);
                            $reviewQuery = $pdo->query("SELECT datePosted, usersID, reviewTitle, reviewText, rating FROM reviews WHERE parkName = '$parkName'");

                            if (isset($_SESSION['user'])) {
                                foreach ($reviewQuery as $review) {
                                    $date = $review["datePosted"];
                                    $user = $review["usersID"];
                                    $title = $review["reviewTitle"];
                                    $text = $review["reviewText"];
                                    $rating = $review["rating"];
                                    echo '
                                        <div class="review-loggedin">
                                            <div id="review-title"><a href="#">',$title,'</a></div>
                                            <div id="stars"><img src="images/',$rating,'stars.png"></div>
                                            <div id="reviewer">',$user,'</div>
                                            <div>',$date,'</div>
                                            <div id="comments">',$text,'</div>
                                        </div>
                                    ';
                                }
                            } else {
                                foreach ($reviewQuery as $review) {
                                    $date = $review["datePosted"];
                                    $user = $review["usersID"];
                                    $title = $review["reviewTitle"];
                                    $text = $review["reviewText"];
                                    $rating = $review["rating"];
                                    echo '
                                        <div class="review-loggedout">
                                            <div id="review-title"><a href="#">',$title,'</a></div>
                                            <div id="stars"><img src="images/',$rating,'stars.png"></div>
                                            <div id="reviewer">',$user,'</div>
                                            <div>',$date,'</div>
                                            <div id="comments">',$text,'</div>
                                        </div>
                                    ';
                                }
                            }
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            echo $parkName;
                        }
                        ?>
                    </div>
                </div>
            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>


        
    </body>
</html>