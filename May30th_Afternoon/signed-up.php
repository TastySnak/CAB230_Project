<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();
    header("Refresh: 2; URL=http://{$_SERVER['HTTP_HOST']}/n9408410/login.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Signup</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>

    <body id="body-signup"> 
        
        <div class="wrapper">

            <!-- Header -->
            <div id="header">
                <div class="nograd">
                    <a id="logo" href="index.php">
                        <img src="images/logo.png" alt="logo">
                    </a>
                    
                </div>
                <div id="h_grad"></div>
            </div>

            <!-- Content -->
            <div class="content-form">
                <!-- Message Box -->
                <div id="message-box">
                    <div class="field-label">Congratulations!</div><br>
                    <div class="field-label">You have successfully created an account.</div>
                </div>
            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>