<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php  
    session_start();
    include 'pdo.php';    
    if (isset($_SESSION['user'])) {
        header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/signed_in.php");
    }

    if (isset($_POST['Submit'])){
        try {
            $stmt = $pdo->prepare('SELECT * FROM users WHERE userName = :username');
            $stmt->bindValue(':username', $_POST['UserName']);
            $stmt->execute();
            $user = $_POST['UserName'];
            $hashed_password = $stmt->fetchColumn(8);
            $collation = password_verify($_POST['PWord'],$hashed_password);
            if ($collation){
                $_SESSION['user'] = $user;
                header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/index.php");
            } else {
                echo "<br><br><br><br><br><br><br><br><br><br><br>Invalid Password";
            }
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
/*
    // Checks to see if someone is already logged in and redirects to home page if true
    if (isset($_SESSION['user'])) {
        header("Location:index.php");
        exit();
    }

    // Checks to see if the login submit button was clicked
    if (isset($_POST['login'])) {
        $username = $_POST['UserName'];
        $password = $_POST['PassWord'];

        try {
            $userQuery = $pdo->query('SELECT userName, password FROM users WHERE userName == ', $username);
            if ($userQuery->rowCount() > 0) {
                if ($password == 'password') {
                    $_SESSION['user'] = $username;
             
                    // <script type="text/javascript">window.location.href = 'index.php';</script>
                } else {

                    // <script type="text/javascript">window.location.href = 'invalid_password.php';</script>

                }
            } else {      

                // <script type="text/javascript">window.location.href = 'invalid_username.php';</script>
             
            }
            
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }*/
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Log in</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>
    <body id="body-login"> 
        
        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Content -->
            <div class="content-form">
                <form action="login.php" method="post" id="login-form" name="log-form">
                    
                    <div id="login-title">Log in</div>

                    <div class="field-label">Username</div>
                    <div><input class="input-login" id="user-field" type="text" name="UserName" onkeypress="off_alert(this)"></div>

                    <div class="field-label">Password</div>
                    <div><input class="input-login" id="pw-field" type="password" name="PWord" onkeypress="off_alert(this)"></div>

                    <input type="submit" name="Submit" value="Submit" id="login-confirm-button" class="confirm-button" onclick="return login_submit();">
                    <input type="reset" name="Cancel" value="Reset" id="login-cancel-button" class="cancel-button" onclick="login_cancel();">

                    <div id="register"><h1>New users</h1><h2> If you don't have your account, please <a href="signup.php">register</a>.</h2></div>

                </form>

                <!-- Validation error modal -->
                <div id="login-validation-modal" class="modal">
                    <a id="login-validation-close-button" href="javascript:login_validation_close();">Close</a>
                    <div id="login-validation-info"></div>
                </div>
            </div>

            <!-- Footer -->
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>

        </div>
    </body>
</html>