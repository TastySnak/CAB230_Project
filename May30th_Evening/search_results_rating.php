<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();
    include 'pdo.php';

    // Global array to hold coords of each location
    $GLOBALS['locations'] = array();
    $coords = array();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Results</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
        <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpfbaVQFDHZJZPwtT_PLCHa63xB6Gg2JM&callback=initResultsMap"></script>

    </head>
    <body id="body-results">       
        <div class="wrapper">
            
            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Content -->
            <div class="inner-content">
                
                <div id="results-droplist">
                    <div id="search-title" class="title">Search Results</div>
                    <div class="results-dropdown-content">
                        <?php
                            $rating = $_GET['rating'];

                            try {
                                $nameQuery = $pdo->prepare("SELECT parkName FROM reviews WHERE rating = :rating");
                                $nameQuery->bindParam(':rating', $rating);
                                $nameQuery->execute();

                                foreach ($nameQuery as $parkName) {
                                    $coords = array();
                                    $park_name = $parkName['parkName'];

                                    $coordQuery = $pdo->prepare("SELECT longitude, latitude FROM parks WHERE Name = :park_name");
                                    $coordQuery->bindParam(':park_name', $park_name);
                                    $coordQuery->execute();

                                    foreach ($coordQuery as $coord) {
                                        array_push($coords, $coord['latitude']);
                                        array_push($coords, $coord['longitude']);
                                    }

                                    $park = array($park_name, "{lat: $coords[0], lng: $coords[1]}");
                                    array_push($GLOBALS['locations'], $park);
                                    echo "<input type=\"submit\" name=\"$park_name\", value=\"$park_name\" onclick=\"window.location='review.php?park=$park_name'\" />";                                    
                                }
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                            }
                        ?>
                    </div>
                </div>

                <div id="results-map-border">
                    <div id="results-map"></div>
                </div>
            </div>

            <!-- Footer -->
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
        <!--
        <script>
            function initResultsMap() {
                var infoWindow;
                var map = new google.maps.Map(document.getElementById('results-map'));
                infoWindow = new google.maps.InfoWindow;


                var bounds = new google.maps.LatLngBounds();

                for (index = 0; index < locations.length; index++) {
                    point = new google.maps.LatLng(locations[index][1]);
                    bounds.extend(point);
                    marker = new google.maps.Marker({
                        position: point,
                        map: map,
                        label: {
                            fontWeight: 'bold',
                            text: locations[index][0]
                        }
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        window.location.href = 'review.php';
                    });
                }

                map.fitBounds(bounds);

                // Find Current Location
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var myPos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        map.setCenter(myPos);
                        //map.setZoom(15);

                        // Add users position marker
                        userMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(myPos.lat, myPos.lng),
                            map: map
                        });
                        userMarker.setIcon('images/green-dot.png');

                    }, function() {
                        handleLocationError(true, infoWindow, map.getCenter());
                        });
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }
            }
        </script>
        -->
    </body>
</html>