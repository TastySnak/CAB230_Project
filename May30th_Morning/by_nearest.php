<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php 
    session_start(); 
    include 'pdo.php';
    include 'functions.php';

    // Hacky way to avoid *Undefined index notice on review page
    // hardcoded placeholder for actual working code
    $park = "ACACIA PARK";

    header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/review.php?park=$park");
    exit();
?>
