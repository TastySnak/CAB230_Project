<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();    
    include 'pdo.php';

    // Check if user is logged in, if so redirect to home page.
    if (isset($_SESSION['user'])) {
        header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/signed_in.php");
        exit();
    }

    // Array of months
    $months = array(
        1 => 'January', 
        2 => 'February', 
        3 => 'March', 
        4 =>'April', 
        5 => 'May', 
        6 => 'June', 
        7 => 'July', 
        8 => 'August', 
        9 => 'September', 
        10 => 'October', 
        11 => 'November', 
        12 => 'December'
    );

	if (isset($_POST['Submit'])){
		try {				
			$stmt = $pdo->prepare('INSERT INTO users (userName, givenName, lastName, day, month, year, emailAddress, password) VALUES (:userName, :givenName, :lastName, :day, :month, :year, :emailAddress, :password)');
			
			$hash = password_hash($_POST['password1'], PASSWORD_DEFAULT);
			
			$stmt->bindValue(':userName', $_POST['username']);
			$stmt->bindValue(':givenName', $_POST['given']);
			$stmt->bindValue(':lastName', $_POST['last']);
			$stmt->bindValue(':day', $_POST['day']);
			$stmt->bindValue(':month', $_POST['month']);
            $stmt->bindValue(':year', $_POST['year']);
            $stmt->bindValue(':emailAddress', $_POST['email']);
            $stmt->bindValue(':password', $hash);

			$stmt->execute();

            // Redirect to success page
            header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/signed-up.php");
            exit();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}					
	}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Signup</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script>
    </head>

    <body id="body-signup"> 
        
        <div class="wrapper">

            <!-- Header -->
            <?php
                include 'header-logged-out.php';
            ?>

            <!-- Content -->
            <div class="content-form">
                <form id="signup-form" name="reg-form" action="signup.php" method="POST">

                    <div id="regist-title">Create an account</div>

                    <div class="field-label">Username</div>
                    <div><input id="uname-field" class="input-signup" type = "text" name="username" onkeypress="off_alert(this)"></div>

                    <div class="field-label">Password</div>
                    <div><input id="passwd1-field" class="input-signup"  type = "password" name="password1" onkeypress="off_alert(this)"></div>

                    <div class="field-label">Re-enter Password</div>
                    <div><input id="passwd2-field" class="input-signup"  type = "password" name="password2" onkeypress="off_alert(this)"></div>

                    <div id="name-label1">Given name</div><div id="name-label2">Surname</div>
                    <div class="name-field"><input id="fname-field" class="n-field" type="text" name="given" onkeypress="off_alert(this)"></div>
                    <div class="name-field"><input id="lname-field" class="n-field" type="text" name="last" onkeypress="off_alert(this)"></div>
                    
                    <div class="field-label">Date of Birth</div>
                    <div id="date-fields">
                        <div id="days">
                            <select name = "day">
                                <?php
                                    for ($day=1; $day<32; $day++) {
                                        echo "<option value=\"$day\">$day</option>";
                                    }
                                ?>
                            </select>
                            <div class="separator">/</div>
                        </div>
                        <div id="months">
                            <select name="month">
                                <?php
                                    foreach ($months as $month_value => $month_text) {
                                        echo "<option value=\"$month_value\">$month_text</option>";
                                    }
                                ?>
                            </select>
                            <div class="separator">/</div>
                        </div>
                        <div id="years">
                            <select name="year" autocomplete="off">
                                <?php
                                    for ($year=1918; $year<2018; $year++) {
                                        if ($year == 2000) {
                                            echo "<option value=\"$year\" selected>$year</option>";
                                        } else {
                                            echo "<option value=\"$year\">$year</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="field-label">Email Address</div>
                    <div><input id="email-field"  class="input-signup" type = "text" name="email" placeholder = "email@address.com" onkeypress="off_alert(this)"></div>
                    
                    <div id="terms">By creating an account, you must accept<br>Brisbane Park Finder's <a id="tnc" href="javascript:terms_function();">terms and conditions.</a><input id="c_box" type="checkbox" name="check" onclick="off_alert(this)"></div>

                    <input type="submit" name="Submit" value="Submit" id="signup-confirm-button" class="confirm-button" onclick="return signup_submit();">
                    <input type="reset" value="Cancel" id="signup-cancel-button" class="cancel-button" onclick="signup_cancel();">
                </form>

                <!-- Term and conditions modal -->
                <div id="terms_conditions-modal" class="modal">
                    <div id="terms_background">
                        <p id="terms-form-title">Terms and Conditions</p>
                        <p>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</p>
                        <p>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>
                        <p>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</p>
                        <p>Your continued use of the Web site constitutes your agreement to all such terms, conditions and notices, and any changes to the Terms and Conditions made by the website owners.</p><br>
                        <a id="terms-close-button" href="javascript:terms_close();">Close</a>
                    </div>
                </div>

                <!-- Validation error modal -->
                <div id="signup-validation-modal" class="modal">
                    <div id="signup-validation-info"></div>
                    <a id="signup-validation-close-button" href="javascript:signup_validation_close();">Close</a>
                </div>
            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>