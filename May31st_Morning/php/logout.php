<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();
    echo "Logout successful";

    // destroys all session data
    session_destroy();

    header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/php/login.php");
?>