<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();
    include 'pdo.php';

    // Check if user is logged in, if not redirect to home page.
    if (!isset($_SESSION['user'])) {
        header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/index.php");
    }

    // Submit review
    if (isset($_POST['review-submit'])){
		try {
            // Retrieve unsubmitted data
            $date = date("Y-m-d H:i:s");
            $park = $_SESSION['park'];
            $user = $_SESSION['user'];

            // Prepare data to be sent to database
			$rev = $pdo->prepare('INSERT INTO reviews (parkName, datePosted, usersID, reviewTitle, reviewText, rating) VALUES (:park, :datePosted, :usersID, :rev_title, :rev_content, :rating)');
			
            // Bind values to variables
			$rev->bindValue(':park', $park);
			$rev->bindValue(':datePosted', $date);
			$rev->bindValue(':usersID', $user);
			$rev->bindValue(':rev_title', $_POST['rev_title']);
			$rev->bindValue(':rev_content', $_POST['rev_content']);
			$rev->bindValue(':rating', $_POST['rating']);

            // Send to database
			$rev->execute();

            // Redirect to review page
            header("Location: http://{$_SERVER['HTTP_HOST']}/n9408410/index.php");
		} catch (PDOException $e) {
			echo $e->getMessage();
		}					
	}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - User Review Page</title>
        <link href="../css/project.css" rel="stylesheet" type="text/css" />
        <script async src="js/project.js" type="text/javascript"></script>
    </head>

    <body id="body-user-rev"> 
        
        <div class="wrapper">

            <!-- Header -->
            <?php
                include "header-logged-in.php";
            ?>

            <!-- Content -->
            <div class="review-content-area">
                <form id="user-review-form" name="rev-form" action="user_review.php" method="POST">

                    <div id="regist-title">Write a review</div>

                    <div class="field-label">Review Title</div>
                    <div><input id="review-title-field" type="text" name="rev_title" placeholder="Enter title here..." onkeypress="off_alert(this)" required>

                    <div class="field-label">Review Rating</div>
                    <div id="review-buttons" class="rating_choice">
                        <input id="rating5" type="radio" name="rating" value="5">
                        <label for="rating5"></label>
                        <input id="rating4" type="radio" name="rating" value="4">
                        <label for="rating4"></label>
                        <input id="rating3" type="radio" name="rating" value="3" checked>
                        <label for="rating3"></label>
                        <input id="rating2" type="radio" name="rating" value="2">
                        <label for="rating2"></label>
                        <input id="rating1" type="radio" name="rating" value="1">
                        <label for="rating1"></label>
                    </div>

                    <div class="field-label">Review Content</div>
                    <div><textarea rows="10" cols="50" id="review-content-field" type="text" name="rev_content" placeholder="Enter review here..." onkeypress="off_alert(this)" required></textarea></div>

                    <input type="submit" value="Submit" id="review-confirm-button" class="confirm-button" name="review-submit" onclick="return signup_submit();">
                    <input type="reset" value="Cancel" id="review-cancel-button" class="cancel-button" name="review-cancel" onclick="window.location='../index.php'">
                    
                </form>

            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>
    </body>
</html>