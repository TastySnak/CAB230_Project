
///////////////////////////
/* Index page javascript */
///////////////////////////

// Auto-find functionality
auto_btn = document.getElementById("auto-button");

// When user clicks 'Closest to you' it goes to the closest park
auto_btn.onclick = function() {
    location.href="review.html";
}

// name-confirm-button functionality
n_conf_btn = document.getElementById("name-confirm-button");
n_conf_btn.onclick = function() {
    location.href="review.html";
}

// name-cancel-button functionality
n_can_btn = document.getElementById("name-cancel-button");
n_can_btn.onclick = function() {
    modal = document.getElementById('modal-p');
    document.getElementById("input-search").value="";
    modal.style.display = "none";
    form.style.display = "block";
}

// rating-confirm-button functionality
r_conf_btn = document.getElementById("rating-confirm-button");
r_conf_btn.onclick = function() {
    location.href="search_results.html";
}

// rating-cancel-button functionality
r_can_btn = document.getElementById("rating-cancel-button");
r_can_btn.onclick = function() {
    modal = document.getElementById('modal-r');
    modal.style.display = "none";
    form.style.display = "block";
}

// Get the modal
var form, modal, auto_btn, sub_btn, park_btn, rate_btn;

// Get the button that opens the modal
form = document.getElementById("searchform");
sub_btn = document.getElementById("suburb-button");
park_btn = document.getElementById("park-button");
rate_btn = document.getElementById("rating-button");

// When the user clicks the a button, open the modal 
sub_btn.onclick = function() {
    modal = document.getElementById('modal-s');
    modal.style.display = "block";
    form.style.display = "none";
}

park_btn.onclick = function() {
    modal = document.getElementById('modal-p');
    modal.style.display = "block";
    form.style.display = "none";
}

rate_btn.onclick = function() {
    modal = document.getElementById('modal-r');
    modal.style.display = "block";
    form.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        form.style.display = "block";
        document.getElementById("input-search").value="";
    }
}

////////////////////////////
/* Signup page javascrpit */
////////////////////////////

// submit-button functionality
n_conf_btn = document.getElementById("signup-confirm-button");
n_conf_btn.onclick = function() {
    location.href="index.html";
}

// cancel-button functionality
n_can_btn = document.getElementById("signup-cancel-button");
n_can_btn.onclick = function() {
    document.getElementById("uname-field").value="";
    document.getElementById("passwd1-field").value="";
    document.getElementById("passwd2-field").value="";
    document.getElementById("fname-field").value="";
    document.getElementById("lname-field").value="";
    document.getElementById("email-field").value="";
}

////////////////////////////
/* Login page javascrpit  */
////////////////////////////

// submit-button functionality
n_conf_btn = document.getElementById("login-confirm-button");
n_conf_btn.onclick = function() {
    location.href="index.html";
}

// cancel-button functionality
n_can_btn = document.getElementById("login-cancel-button");
n_can_btn.onclick = function() {
    document.getElementById("user-field").value="";
    document.getElementById("pw-field").value="";
}

////////////////////////////////////
/* Search_Results page javascrpit */
////////////////////////////////////

function initResultsMap() {
    var ray_lat = -27.48076499;
    var ray_long = 153.0387956;
    var ray = {lat: ray_lat, lng: ray_long};

    var bot_lat = -27.47578578;
    var bot_long = 153.0300306;
    var botanic = {lat: bot_lat, lng: bot_long};

    var mar_lat = -27.48194089;
    var mar_long = 153.0264731;
    var mar = {lat: mar_lat, lng: mar_long};

    var map = new google.maps.Map(document.getElementById('results-map'), {
        zoom: 15,
        center: botanic
    });

    var marker1 = new google.maps.Marker({
        position: ray,
        map: map
    });
    
    var marker2 = new google.maps.Marker({
        position: botanic,
        map: map
    });

    var marker3 = new google.maps.Marker({
        position: mar,
        map: map
    });
}

////////////////////////////
/* Review page javascrpit */
////////////////////////////

function initReviewMap() {

    var bot_lat = -27.47578578;
    var bot_long = 153.0300306;
    var botanic = {lat: bot_lat, lng: bot_long};

    var map = new google.maps.Map(document.getElementById('review-map'), {
        zoom: 16,
        center: botanic
    });
    
    var marker1 = new google.maps.Marker({
        position: botanic,
        map: map
    });        
}