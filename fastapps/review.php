<!DOCTYPE html>
<!-- 
    CAB230 Project Authors:
    Gary Murphy   n9408410
    Aki Maruyama  n9534041
-->

<?php
    session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Brisbane Park Finder - Reviews</title>
        <link href="project.css" rel="stylesheet" type="text/css" />
        <script async src="project.js" type="text/javascript"></script> 
        <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpfbaVQFDHZJZPwtT_PLCHa63xB6Gg2JM&callback=initReviewMap"></script>
    </head>
    <body id="body-review"> 
        <div class="wrapper">

            <!-- Header -->
            <?php
                if (isset($_SESSION['user'])) {
                    include 'header-logged-in.php';
                } else {
                    include 'header-logged-out.php';
                }
            ?>

            <!-- Content -->
            <div class="inner-content">
                <div id="info-box">                    
                    <div class="title">Park Information</div>
                    <div id= "park-details">City Botanical Gardens<br>Alice Street, Brisbane</div>
                    <div id="review-map"></div>
                </div>

                <div id="review-box">
                    <?php
                        $_SESSION['park'] = $_GET['park'];  // This shows a notice if page is accessed directly from index.php *Undefined index                            
                        
                        if (isset($_SESSION['user'])) {            
                            echo '<div class="title">User Reviews</div>';
                            echo '<input type="submit" name="leave-review" value="Submit Review" id="leave-review-button" onclick="window.location=\'user_review.php\'" >';
                            echo '<div id="review-list-loggedin">';
                        } else {            
                            echo '<div class="title">User Reviews</div>';
                            echo '<div id="review-list-loggedout">';
                        }
                    ?>

                    <!-- Hard-coded Reviews -->
                    <?php
                        if (isset($_SESSION['user'])) {
                            echo '
                            <div class="review-loggedin">
                                <div id="review-title"><a href="#">Great park. Close to city.</a></div>
                                <div id="stars"><img src="images/4stars.png" alt="4 stars"></div>
                                <div id="reviewer">Kevin Stein</div>
                                <div id="comments">
                                    Maecenas ac eleifend dui. Pellentesque dictum justo efficitur condimentum tristique. Nulla eu urna efficitur, suscipit ex ultricies, imperdiet magna. Curabitur lacinia libero sed tincidunt consectetur. Ut nec libero eu est malesuada facilisis. Nullam tempor enim commodo lorem auctor fermentum.
                                </div>
                            </div>
                            <div class="review-loggedin">
                                <div id="review-title"><a href="#">Relaxing city getaway.</a></div>
                                <div id="stars"><img src="images/5stars.png" alt="5 stars"></div>
                                <div id="reviewer">Lucy Drysdale</div>
                                <div id="comments">
                                    Maecenas ac eleifend dui. Pellentesque dictum justo efficitur condimentum tristique. Nulla eu urna efficitur, suscipit ex ultricies, imperdiet magna. Curabitur lacinia libero sed tincidunt consectetur. Ut nec libero eu est malesuada facilisis. Nullam tempor enim commodo lorem auctor fermentum.
                                </div>
                            </div>
                            <div class="review-loggedin">
                                <div id="review-title"><a href="#">Too many ibises.</a></div>
                                <div id="stars"><img src="images/1stars.png" alt="1 star"></div>
                                <div id="reviewer">Jason Chan</div>
                                <div id="comments">
                                    Maecenas ac eleifend dui. Pellentesque dictum justo efficitur condimentum tristique. Nulla eu urna efficitur, suscipit ex ultricies, imperdiet magna. Curabitur lacinia libero sed tincidunt consectetur. Ut nec libero eu est malesuada facilisis. Nullam tempor enim commodo lorem auctor fermentum.
                                </div>
                            </div>
                            ';
                        } else {
                            echo '
                            <div class="review-loggedout">
                                <div id="review-title"><a href="#">Great park. Close to city.</a></div>
                                <div id="stars"><img src="images/4stars.png" alt="4 stars"></div>
                                <div id="reviewer">Kevin Stein</div>
                                <div id="comments">
                                    Maecenas ac eleifend dui. Pellentesque dictum justo efficitur condimentum tristique. Nulla eu urna efficitur, suscipit ex ultricies, imperdiet magna. Curabitur lacinia libero sed tincidunt consectetur. Ut nec libero eu est malesuada facilisis. Nullam tempor enim commodo lorem auctor fermentum.
                                </div>
                            </div>
                            <div class="review-loggedout">
                                <div id="review-title"><a href="#">Relaxing city getaway.</a></div>
                                <div id="stars"><img src="images/5stars.png" alt="5 stars"></div>
                                <div id="reviewer">Lucy Drysdale</div>
                                <div id="comments">
                                    Maecenas ac eleifend dui. Pellentesque dictum justo efficitur condimentum tristique. Nulla eu urna efficitur, suscipit ex ultricies, imperdiet magna. Curabitur lacinia libero sed tincidunt consectetur. Ut nec libero eu est malesuada facilisis. Nullam tempor enim commodo lorem auctor fermentum.
                                </div>
                            </div>
                            <div class="review-loggedout">
                                <div id="review-title"><a href="#">Too many ibises.</a></div>
                                <div id="stars"><img src="images/1stars.png" alt="1 star"></div>
                                <div id="reviewer">Jason Chan</div>
                                <div id="comments">
                                    Maecenas ac eleifend dui. Pellentesque dictum justo efficitur condimentum tristique. Nulla eu urna efficitur, suscipit ex ultricies, imperdiet magna. Curabitur lacinia libero sed tincidunt consectetur. Ut nec libero eu est malesuada facilisis. Nullam tempor enim commodo lorem auctor fermentum.
                                </div>
                            </div>
                            ';
                        }
                        ?>
                    </div>
                </div>
            </div>

            <!-- Footer -->  
            <div class="footer">
                <div id="f_grad"></div>
                <div class="nograd"></div>
            </div>
        </div>


        
    </body>
</html>